import data_handler
import itertools
import numpy as np
import solver
import sys
import time

if __name__ == "__main__":  
    num_islands = 100
    num_random_games = 30
    alpha = sys.argv[1]
    beta = sys.argv[2]
    #alphas = ["00", "05", "10", "15"]
    #betas = ["25", "50", "75"]

    result_filepath_template = "result/Hs_16_{0}_{1}_{2}.tsv"
    result_filepath = result_filepath_template.format(num_islands, beta, alpha)
    results = np.zeros((6, num_random_games))

    handler = data_handler.DataHandler("dataset/Hashi_Puzzles/")
    with open(result_filepath, "w") as fp:
        for i in range(num_random_games):
            num = ("0" if i+1 < 10 else "") + str(i+1)
            filepath_template = "{0}/Hs_16_{0}_{1}_{2}_0{3}.has"
            filepath = filepath_template.format(num_islands, beta, alpha, num)
            grid = handler.load_puzzle(filepath)
            
            start = time.time()
            puzzle = solver.Puzzle(grid)
            game = solver.Game(puzzle)
            aco = solver.ACO(game, num_samples=100, num_epoches=20, verbose=False)
            aco.run()
            delta = time.time() - start

            results[0, i] = puzzle.num_islands
            results[1, i] = puzzle.num_total_bridges_required
            results[2, i] = aco.best_of_the_best_game.get_largest_graph_component()
            results[3, i] = aco.best_of_the_best_game.num_completed_islands
            results[4, i] = aco.best_of_the_best_game.num_total_built_bridges
            results[5, i] = delta

            row_template = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}"
            row = row_template.format(filepath, results[0, i], results[1, i], results[2, i], 
                                        results[3, i], results[4, i], results[5, i])
            fp.write(row + "\n")                
            print(i, row)

    with open("result/aggregated_results.tsv", "a") as fp:
        num_found_solutions = 0
        for i in range(num_random_games):
            is_connected_graph = results[0, i] == results[2, i]
            is_completed_islands = results[0, i] == results[3, i]
            is_built_all_bridges = results[1, i] == results[4, i]

            found_solution = is_connected_graph and is_completed_islands and is_built_all_bridges
            if found_solution:
                num_found_solutions += 1
        result_template = "{0}\t{1}\t{2}\t{3}"
        result = result_template.format(alpha, beta, num_found_solutions, results[5].mean())
        fp.write(result + "\n")
        print(result)

    

            
            