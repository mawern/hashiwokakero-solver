import data_handler
import solver
import time

if __name__ == "__main__":
    handler = data_handler.DataHandler("dataset/Hashi_Puzzles/")
    grid = handler.load_puzzle("100/Hs_16_100_75_15_030.has")
    #grid = handler.load_puzzle("200/Hs_24_200_75_15_030.has")
    #grid = handler.load_puzzle("300/Hs_29_200_75_15_030.has")
    #grid = handler.load_puzzle("400/Hs_34_400_75_15_030.has")
    start = time.time()
    puzzle = solver.Puzzle(grid)
    #puzzle.print_auxiliary_data_structures()
    game = solver.Game(puzzle)
    aco = solver.ACO(game)
    aco.run()
    delta = time.time() - start
    aco.best_of_the_best_game.print_solution()
    aco.best_of_the_best_game.print_solution_statistics()
    print("\nTotal time: {0}\n".format(delta))
