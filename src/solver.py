from enum import IntEnum

import copy
import math
import numpy as np

class Direction(IntEnum):
    NORTH = 0
    SOUTH = 1
    EAST = 2
    WEST = 3

class Puzzle:

    def __init__(self, grid):
        self.grid = grid
        self.grid_width, self.grid_height = grid.shape
        self.positions = [(x, y) for x, y in zip(*grid.nonzero())]
        self.num_islands = len(self.positions)

        self.bridges_required_map = grid[grid.nonzero()]
        self.num_total_bridges_required = self.bridges_required_map.sum()
        self.indices = {(x, y) : idx
                        for idx, (x, y) in enumerate(self.positions)}

        self._initialize_auxiliary_data_structures()

    def _initialize_auxiliary_data_structures(self):

        # Get adjacent islands
        self.nearest_neighbors_map = self.num_islands * [None]
        for idx in range(self.num_islands):
            x, y = self.positions[idx]
            self.nearest_neighbors_map[idx] = self._check_nearest_neighbors(x, y)

        # Get blocked bridges by other bridges
        self.blocked_bridges_per_bridge_map = dict()
        for idx1 in range(self.num_islands):
            for idx2 in self.nearest_neighbors_map[idx1]:
                if idx1 > idx2:
                    continue
                self.blocked_bridges_per_bridge_map[(idx1, idx2)] = self._check_blocked_bridges(idx1, idx2)    

    def _check_nearest_neighbors(self, x, y):
        nearest_neighbors = [-1, -1, -1, -1]

        # Check north
        for i in range(x - 1, -1, -1):
            if self.grid[i, y] == 0:
                continue
            nearest_neighbors[Direction.NORTH] = self.indices[(i, y)]
            break
                
        # Check south
        for i in range(x + 1, self.grid_height):
            if self.grid[i, y] == 0:
                continue
            nearest_neighbors[Direction.SOUTH] = self.indices[(i, y)]
            break

        # Check east
        for j in range(y - 1, -1, -1):
            if self.grid[x, j] == 0:
                continue
            nearest_neighbors[Direction.EAST] = self.indices[(x, j)]
            break
                
        # Check west
        for j in range(y + 1, self.grid_width):
            if self.grid[x, j] == 0:
                continue
            nearest_neighbors[Direction.WEST] = self.indices[(x, j)]
            break

        return nearest_neighbors

    def _check_blocked_bridges(self, idx1, idx2):
        x1, y1 = self.positions[idx1]
        x2, y2 = self.positions[idx2]

        x_min, x_max = (x1, x2) if x1 < x2 else (x2, x1)
        y_min, y_max = (y1, y2) if y1 < y2 else (y2, y1)

        blocked_bridges = []
        for idx in range(self.num_islands):
            if idx == idx1 or idx == idx2:
                continue

            # We just need to check from one of the blocked bridge endpoints
            i1, j1 = self.positions[idx]

            # Check from the Northen endpoint
            if (j1 > y_min and j1 < y_max) and i1 < x_min:
                # Check if there is an island in the west
                idx_temp = self.nearest_neighbors_map[idx][Direction.SOUTH]
                if idx_temp < 0:
                    continue
                # Check if the island in the northen crosses the bridge between idx1 and idx2
                i2, j2 = self.positions[idx_temp]
                if i2 > x_max:
                    blocked_bridges.append((idx, idx_temp))

            # Check from the Easten endpoint
            elif (i1 > x_min and i1 < x_max) and j1 < y_min:
                # Check if there is an island in the south
                idx_temp = self.nearest_neighbors_map[idx][Direction.WEST]
                if idx_temp < 0:
                    continue
                # Check if the island in the easten crosses the bridge between idx1 and idx2
                i2, j2 = self.positions[idx_temp]
                if j2 > y_max:
                    blocked_bridges.append((idx, idx_temp))            

        return blocked_bridges    

    def print_auxiliary_data_structures(self):
        print("\n##### ADJACENT ISLANDS #####\n")
        for idx in range(self.num_islands):
            nearest_neighbors = self.nearest_neighbors_map[idx]
            idx_north = nearest_neighbors[Direction.NORTH]
            idx_south = nearest_neighbors[Direction.SOUTH]
            idx_east = nearest_neighbors[Direction.EAST]
            idx_west = nearest_neighbors[Direction.WEST]
            print("{0} -> \t {1} \t {2} \t {3} \t {4}".format(self.positions[idx],
                                                              self.positions[idx_north] if idx_north >= 0 else (-1, -1),
                                                              self.positions[idx_south] if idx_south >= 0 else (-1, -1),
                                                              self.positions[idx_east] if idx_east >= 0 else (-1, -1),
                                                              self.positions[idx_west] if idx_west >= 0 else (-1, -1)))

        print("\n##### BRIDGES BLOCKED BY EACH BRIDGE #####\n")
        for key, value in self.blocked_bridges_per_bridge_map.items():
            if not value:
                continue
            idx1, idx2 = key 
            x1, y1 = self.positions[idx1]
            x2, y2 = self.positions[idx2]

            text = ""
            for idx3, idx4 in value:
                i1, j1 = self.positions[idx3]
                i2, j2 = self.positions[idx4]
                text += "[{0} ~ {1}]\t".format((i1, j1), (i2, j2))
            print("[{0} ~ {1}] -> {2}".format((x1, y1), (x2, y2), text))

class Game:

    def __init__(self, puzzle, use_heuristic=True):
        self.puzzle = puzzle
        self.remaining_bridges_map = np.copy(puzzle.bridges_required_map)
        self.available_nearest_neighbors_map = [[idx for idx in nearest_neighbors if idx >= 0] 
                                                for nearest_neighbors in self.puzzle.nearest_neighbors_map]
        self.built_bridges_per_island_pair_map = dict()
        self.num_total_built_bridges = 0
        self.num_completed_islands = 0

        if use_heuristic:
            self._run_heuristic()

    def _run_heuristic(self, ommit_errors=False):
        has_more_updates = True
        while has_more_updates:
            #all_islands_to_be_updated = set(range(self.puzzle.num_islands))
            all_islands_to_be_updated = set([idx for idx, num_remaining_bridges in enumerate(self.remaining_bridges_map) 
                                             if num_remaining_bridges > 0])
            has_more_updates = False
            while len(all_islands_to_be_updated) != 0:
                idx = all_islands_to_be_updated.pop()
                local_islands_to_be_updated = set()
                try:
                    local_islands_to_be_updated = self._apply_basic_techniques(idx)
                    local_islands_to_be_updated ^= self._apply_fix_techniques(idx)
                except Exception as e:
                    if not ommit_errors:
                        raise e
                if len(local_islands_to_be_updated) == 0:
                    continue
                all_islands_to_be_updated ^= local_islands_to_be_updated
                has_more_updates = True

    def _apply_basic_techniques(self, idx):
        num_remaining_bridges = self.remaining_bridges_map[idx]

        # Must copy beucase data structure will be updated inside "build_bridges" function
        nearest_neighbors = np.copy(self.available_nearest_neighbors_map[idx])

        islands_to_be_updated = set()
        
        num_islands_needed = math.ceil(num_remaining_bridges / 2)
        # Number of adjacent islands cannot not be less than needed -> Implementation error
        if len(nearest_neighbors) < num_islands_needed:
            raise ValueError("ERROR! NUMBER OF ADJACENT ISLANDS MUST BE {0}".format(num_islands_needed))
        # If there is more islands than needed is uncertain which to connect
        elif len(nearest_neighbors) > num_islands_needed:
            return islands_to_be_updated

        num_bridges_to_built = 2 if num_remaining_bridges % 2 == 0 else 1
        for idx_temp in nearest_neighbors:
            islands_to_be_updated ^= self._build_bridges(idx, idx_temp, num_bridges_to_built)

        #if num_remaining_bridges % 2 == 1:
        #    islands_to_be_updated.add(idx)

        return islands_to_be_updated

    def _apply_fix_techniques(self, idx):
        # Must copy beucase data structure will be updated inside "build_bridges" function
        nearest_neighbors = np.copy(self.available_nearest_neighbors_map[idx])

        num_total_bridges, num_islands_with_1 = 0, 0
        for idx_temp in nearest_neighbors:
            key = (idx, idx_temp) if idx < idx_temp else (idx_temp, idx)
            is_already_built = key in self.built_bridges_per_island_pair_map
            num_already_built = self.built_bridges_per_island_pair_map[key] if is_already_built else 0

            num_remaining_bridges_to_neighbor = 2 - num_already_built
            if num_remaining_bridges_to_neighbor == 1:
                num_islands_with_1 += 1
            num_total_bridges += num_remaining_bridges_to_neighbor

        islands_to_be_updated = set()

        # Number of adjacent islands cannot not be less than needed -> Implementation error
        num_remaining_bridges = self.remaining_bridges_map[idx]
        if num_total_bridges < num_remaining_bridges:
            raise ValueError("ERROR! NUMBER MAXIMUM BRIDGES TO NEIGHBORS IS LESS THAN NEEDED. {0} > {1}".format(num_remaining_bridges, 
                                                                                                                num_total_bridges))
        #elif num_islands_with_1 + 1 < len(nearest_neighbors) or num_total_bridges > num_remaining_bridges:
        elif num_total_bridges > num_remaining_bridges:
            return islands_to_be_updated

        for idx_temp in nearest_neighbors:
            key = (idx, idx_temp) if idx < idx_temp else (idx_temp, idx)
            is_already_built = key in self.built_bridges_per_island_pair_map
            num_already_built = self.built_bridges_per_island_pair_map[key] if is_already_built else 0

            num_bridges_to_built = 2 - num_already_built
            islands_to_be_updated ^= self._build_bridges(idx, idx_temp, num_bridges_to_built)

        return  islands_to_be_updated

    def _build_bridges(self, idx1, idx2, num_bridges):
        key = (idx1, idx2) if idx1 < idx2 else (idx2, idx1)

        # Check if another bridge is already blocking the path
        for key_temp in self.puzzle.blocked_bridges_per_bridge_map[key]:
            if key_temp in self.built_bridges_per_island_pair_map:
                raise RuntimeError("ERROR! THERE IS ALREADY A BRIDGE BUILT IN THE WAY!")

        # Check if not excedding any limit
        is_already_built = key in self.built_bridges_per_island_pair_map
        num_already_built =  self.built_bridges_per_island_pair_map[key] if is_already_built else 0        
        if num_bridges < 1 or num_already_built < 0 or num_bridges + num_already_built > 2:
            raise ValueError("ERROR! MAX BRIDGES EXCEEDED! Built: {0} Requested: {1}".format(num_already_built, num_bridges))
        elif self.remaining_bridges_map[idx1] - num_bridges < 0 or self.remaining_bridges_map[idx2] - num_bridges < 0:
            raise ValueError("ERROR! MAX BRIDGES EXCEEDED IN A INDIVIDUAL ISLAND!")

        self.built_bridges_per_island_pair_map[key] = num_bridges + num_already_built
        self.remaining_bridges_map[idx1] -= num_bridges
        self.remaining_bridges_map[idx2] -= num_bridges
        self.num_total_built_bridges += (2 * num_bridges)

        # Since we built a bridge, "local" islands should be updated
        islands_to_be_updated = set()
        islands_to_be_updated.update(self.available_nearest_neighbors_map[idx1])
        islands_to_be_updated.update(self.available_nearest_neighbors_map[idx2])

        # Since we built a bridge, the blocked bridges cannot be built thus should be remove
        for key_temp in self.puzzle.blocked_bridges_per_bridge_map[key]:
            idx3, idx4 = key_temp
            # TEMP (?) solution
            # Basically more than one bridge can block a same other bridge
            try:
                self.available_nearest_neighbors_map[idx3].remove(idx4)
            except ValueError:
                pass
            try:
                self.available_nearest_neighbors_map[idx4].remove(idx3)
            except ValueError:
                pass
            islands_to_be_updated.update([idx3, idx4])

        # Check if reached max capacity between islands
        # If yes, we cannot built more bridges between them
        if num_bridges + num_already_built == 2:
            self.available_nearest_neighbors_map[idx1].remove(idx2)
            self.available_nearest_neighbors_map[idx2].remove(idx1)
        
        if self.remaining_bridges_map[idx1] == 0:
            for idx_temp in self.available_nearest_neighbors_map[idx1]:
                self.available_nearest_neighbors_map[idx_temp].remove(idx1)
            self.available_nearest_neighbors_map[idx1].clear()
            self.num_completed_islands += 1
        if self.remaining_bridges_map[idx2] == 0:
            for idx_temp in self.available_nearest_neighbors_map[idx2]:
                self.available_nearest_neighbors_map[idx_temp].remove(idx2)
            self.available_nearest_neighbors_map[idx2].clear()
            self.num_completed_islands += 1

        return islands_to_be_updated

    def print_solution_statistics(self):
        print("\n##### STATISTICS #####\n")
        print("Built {0} of {1} bridges".format(self.num_total_built_bridges, self.puzzle.num_total_bridges_required))
        print("Completed {0} of {1} islands".format(self.num_completed_islands, self.puzzle.num_islands))

    def print_solution(self):
        solution = np.copy(self.puzzle.grid).astype(str)
        solution[solution == '0'] = ' '
        for key, value in self.built_bridges_per_island_pair_map.items():
            idx1, idx2 = key
            x1, y1 = self.puzzle.positions[idx1]
            x2, y2 = self.puzzle.positions[idx2]

            x_min, x_max = (x1, x2) if x1 < x2 else (x2, x1)
            y_min, y_max = (y1, y2) if y1 < y2 else (y2, y1)
            if x_min == x_max:
                for j in range(y_min+1, y_max):
                    solution[x_min, j] = '-' if value == 1 else '='
            else:
                for i in range(x_min+1, x_max):
                    solution[i, y_min] = '-' if value == 1 else '='
        print("\n##### SOLUTION #####\n")
        print(solution)

        print("\n##### CONNECTIONS #####\n")
        print([[self.puzzle.positions[idx1], self.puzzle.positions[idx2], value] 
                for (idx1, idx2), value in self.built_bridges_per_island_pair_map.items()])
        
        print("\n##### NUM REMAINING BRIDGES #####\n")
        print([[self.puzzle.positions[idx], num_remaining_bridges]
               for idx, num_remaining_bridges in enumerate(self.remaining_bridges_map)])

    def clone_solution(self):
        cloned_game = Game(self.puzzle, False)
        cloned_game.remaining_bridges_map = np.copy(self.remaining_bridges_map)
        cloned_game.available_nearest_neighbors_map = copy.deepcopy(self.available_nearest_neighbors_map)
        cloned_game.built_bridges_per_island_pair_map = copy.deepcopy(self.built_bridges_per_island_pair_map)
        cloned_game.num_total_built_bridges = self.num_total_built_bridges
        cloned_game.num_completed_islands = self.num_completed_islands        

        return cloned_game

    def build_bridges(self, idx1, idx2, num_bridges):
        self._build_bridges(idx1, idx2, num_bridges)
        self._run_heuristic(True)

    def try_to_build_bridges(self, idx1, idx2):
        if idx2 not in self.available_nearest_neighbors_map[idx1]:
            return 
        #print(self.puzzle.positions[idx1], self.puzzle.positions[idx2])
        key = (idx1, idx2) if idx1 < idx2 else (idx2, idx1)        

        num_bridges_to_built = 2
        if key in self.built_bridges_per_island_pair_map:
            num_bridges_already_built = self.built_bridges_per_island_pair_map[key]
            num_bridges_to_built -= num_bridges_already_built
        num_bridges_to_built = min([num_bridges_to_built, 
                                    self.remaining_bridges_map[idx1], 
                                    self.remaining_bridges_map[idx2]])
        if num_bridges_to_built < 1:
            return
        self.build_bridges(idx1, idx2, 1)

    def get_largest_graph_component(self):
        # Build graph
        graph = [set() for _ in range(self.puzzle.num_islands)]
        for idx1, idx2 in self.built_bridges_per_island_pair_map.keys():
            graph[idx1].add(idx2)
            graph[idx2].add(idx1)

        # Check if connected
        component_idx = 0
        visited = [-1] * self.puzzle.num_islands        
        for i in range(self.puzzle.num_islands):
            if visited[i] >= 0:
                continue
            visited[i] = component_idx
            islands_to_visit = [i]
            while len(islands_to_visit) != 0:
                idx = islands_to_visit.pop(0)
                neighbors = graph[idx]
                for neighbor in neighbors:
                    if visited[neighbor] >= 0:
                        continue
                    visited[neighbor] = component_idx
                    islands_to_visit.append(neighbor)
            component_idx += 1
        num_islands_by_component = np.bincount(visited)
        return max(num_islands_by_component)

    def check_if_connected_graph(self):
        return self.get_largest_graph_component() == self.puzzle.num_islands

class ACO:

    def __init__(self, base_game, num_samples=50, num_epoches=20, verbose=True):
        self.base_game = base_game
        self.num_samples = num_samples
        self.num_epoches = num_epoches
        self.verbose = verbose
        self.best_of_the_best_game = self.base_game
        self.best_of_the_best_game_largest_component = self.base_game.get_largest_graph_component()

        # ACO parameters
        self.rho = 0.9
        self.gamma = 0.1

        # Get all possible bridges between islands
        self.all_possible_bridges_to_built = {(idx1, idx2) if idx1 < idx2 else (idx2, idx1)
                                              for idx1, indices in enumerate(self.base_game.available_nearest_neighbors_map)
                                              for idx2 in indices}
        self.all_possible_bridges_to_built = np.array(list(self.all_possible_bridges_to_built))
        self.num_possible_bridges_to_built = len(self.all_possible_bridges_to_built)
        self.bridge_indices = {tuple(self.all_possible_bridges_to_built[idx]) : idx 
                               for idx in range(self.num_possible_bridges_to_built)}
        self.pheromone_array = 0.5 * np.ones(self.num_possible_bridges_to_built)

    def _apply_evaporation(self):
        self.pheromone_array *= (1 - self.rho)

    def _apply_intensification(self, game):
        num_total_bridges_baseline = self.base_game.num_total_built_bridges
        num_total_bridges_required = self.base_game.puzzle.num_total_bridges_required - num_total_bridges_baseline
        num_total_built_bridges = game.num_total_built_bridges - num_total_bridges_baseline
        delta = self.gamma * (num_total_bridges_required - num_total_built_bridges) / num_total_built_bridges

        indices_to_update = []
        for key, num_bridges_built in game.built_bridges_per_island_pair_map.items():
            if key not in self.bridge_indices:
                continue
            # It's possible that the game already started with some bridges built
            elif key in self.base_game.built_bridges_per_island_pair_map:
                num_bridges_already_built = self.base_game.built_bridges_per_island_pair_map[key]
                if num_bridges_built == num_bridges_already_built:
                    continue
            idx = self.bridge_indices[key]
            indices_to_update.append(idx)
        self.pheromone_array[indices_to_update] += delta        

    def _get_all_bridges_to_built_by_preference(self):
        eta_array = [1 / (self.base_game.remaining_bridges_map[idx1] + self.base_game.remaining_bridges_map[idx2])
                     for idx1, idx2 in self.all_possible_bridges_to_built]        
        eta_array = np.array(eta_array)
        probabilities = (eta_array * self.pheromone_array) / (eta_array * self.pheromone_array).sum()
        size = self.num_possible_bridges_to_built
        sorted_indices = np.random.choice(size, size, p=probabilities, replace=False)
        #print(np.percentile(probabilities, q=[i/10 for i in range(10)] + [1]))
        #print(max(eta_array * self.pheromone_array), min(eta_array * self.pheromone_array))
        return self.all_possible_bridges_to_built[sorted_indices]

    def _get_best_game(self, curr_game, curr_game_largest_component, alt_game):
        largest_component = alt_game.get_largest_graph_component()

        # 1. Prefer games with larger connected components
        if largest_component < curr_game_largest_component:
            return curr_game, curr_game_largest_component
        elif largest_component > curr_game_largest_component:
            if self.verbose:
                print(largest_component, alt_game.num_completed_islands, alt_game.num_total_built_bridges)
            return alt_game.clone_solution(), largest_component
        # 2. Prefer games with larger number of islands completed
        elif alt_game.num_completed_islands < curr_game.num_completed_islands:
            return curr_game, curr_game_largest_component
        elif alt_game.num_completed_islands > curr_game.num_completed_islands:
            if self.verbose:
                print(largest_component, alt_game.num_completed_islands, alt_game.num_total_built_bridges)
            return alt_game.clone_solution(), largest_component            
        # 3. Prefer games with larger number of built bridges
        elif alt_game.num_total_built_bridges > curr_game.num_total_built_bridges:
            if self.verbose:
                print(largest_component, alt_game.num_completed_islands, alt_game.num_total_built_bridges)
            return alt_game.clone_solution(), largest_component
        return curr_game, curr_game_largest_component    
        
    def run(self):
        for i in range(self.num_epoches):
            best_game = self.base_game.clone_solution()
            best_game_largest_component = best_game.get_largest_graph_component()            
            for j in range(self.num_samples):
                game = self.base_game.clone_solution()
                
                for idx1, idx2 in self._get_all_bridges_to_built_by_preference():
                    game.try_to_build_bridges(idx1, idx2)
                best_game, best_game_largest_component = self._get_best_game(best_game, best_game_largest_component, game)

            # Dump information about current generation
            if self.verbose:
                generation_template_msg = "Generation {0} -> Largest component {1} of {2} islands; Completed {3} of {2} islands; Built {4} of {5} bridges."
                print(generation_template_msg.format(i,
                                                     best_game_largest_component,
                                                     best_game.puzzle.num_islands,
                                                     best_game.num_completed_islands,
                                                     best_game.num_total_built_bridges, 
                                                     best_game.puzzle.num_total_bridges_required))

            # Check if game improved
            self.best_of_the_best_game, self.best_of_the_best_game_largest_component = \
                self._get_best_game(self.best_of_the_best_game, self.best_of_the_best_game_largest_component, best_game)

            # Check if found "final" feasible solution
            if (self.best_of_the_best_game.check_if_connected_graph() 
                and self.best_of_the_best_game.num_total_built_bridges == self.best_of_the_best_game.puzzle.num_total_bridges_required):
                return

            # Update ACO data structures
            self._apply_evaporation()
            self._apply_intensification(best_game)