import numpy as np
import os

class DataHandler:

    def __init__(self, rootpath):
        self.rootpath = rootpath

    def load_puzzle(self, filepath):
        with open(os.path.join(self.rootpath, filepath)) as fp:
            # width height num_islands
            w, h, num_islands = fp.readline().split()
            grid = np.loadtxt(fp).astype(np.int8)
            return grid